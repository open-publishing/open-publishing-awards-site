const cssnano = require('cssnano');
const postcssPresetEnv = require('postcss-preset-env');
const postcssNested = require('postcss-nested');
const postcssImport = require('postcss-easy-import');

module.exports = {
  plugins: [
    cssnano(), 
    postcssPresetEnv(), 
    postcssNested(),
    postcssImport()]
};