import axios from "axios";

let config = {
  server: "https://openpublishingfest2.cloud68.co",
  adminEmail: "adam@coko.foundation",
};

window.onload = loadData();

document
  .querySelectorAll(".form input, .form textarea, .form select")
  .forEach((input) => {
    input.addEventListener("change", saveData);
  });

// set up form
const formElement = document.querySelector("#form");

formElement.addEventListener("submit", (e) => {
  e.preventDefault();
  console.log(centerThanks);
  centerThanks();
  const formElements = formElement.elements;
  const honey = document.querySelector(".nah");
  if (honey.value == "") {
    const formContent = new FormData(formElement);

    const data = {};

    for (let i = 0; i < formElements.length; i++) {
      const currentElement = formElements[i];

      if (
        !["submit", "file"].includes(currentElement.type) &&
        currentElement.name != "enrico"
      ) {
        if (
          currentElement.type == "checkbox" &&
          currentElement.checked == true
        ) {
          data[currentElement.name] = "true";
        } else if (
          currentElement.type == "checkbox" &&
          currentElement.checked == false
        ) {
          data[currentElement.name] = "false";
        } else {
          data[currentElement.name] = currentElement.value;
        }
      }
    }
    const stringdata = data;
    formContent.append("data", JSON.stringify(stringdata));
    formContent.delete("enrico");

    axios
      .post(`${config.server}/opa-nominees/`, formContent)
      .then(function (response) {
        document.querySelector(".form").innerHTML =
          '<p class="thanks">Thanks for your proposal, we’ll contact you soon!</p>';
      })
      .then(removeData())
      .catch((error) => {
        console.log(error);
        document.querySelector(".form").innerHTML = `
                <p class="thanks">An error occured:  ${error}</p>
                <p class="thanks">There has been an error while uploading your form, please contact <a href="mailto:${config.adminEmail}">${config.adminEmail}</a>. or <a href="/form/index.html">try again</a>.</p>`;
      })
      .finally(centerThanks());
  }
});

function emailIsValid(email) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

function centerThanks() {
  document
    .querySelector(".form")
    .scrollIntoView({ behavior: "smooth", block: "center" });
}

const emailZone = document.querySelector('input[type="email"]');

function saveData() {
  document
    .querySelectorAll(".form input, .form textarea, .form select")
    .forEach((input) => {
      localStorage.setItem(`OpenAwards-${input.name}`, input.value);
    });
  // console.log("save:", localStorage);
}

function loadData() {
  // load data
  // console.log("load:", localStorage);

  document
    .querySelectorAll(".form input, .form textarea, .form select")
    .forEach((input) => {
      // if (!input.name == `enrico`) {
      if (input.id != "categories") {
        input.value = localStorage.getItem(`OpenAwards-${input.name}`);
      }
      // }
    });
}

function removeData() {
  // remove data when the form is sent

  document
    .querySelectorAll(".form input, .form textarea, .form select")
    .forEach((input) => {
      localStorage.removeItem(`OpenAwards-${input.name}`);
    });
}

document
  .querySelector("#subscribecoko")
  .addEventListener("change", function () {
    if (
      document.querySelector("#email").value < 1 &&
      document.querySelector("#subscribecoko").checked == true
    ) {
      document.querySelector("#subscribecoko").value = "true";
      document.querySelector("#email").focus();

      document
        .querySelector("#subscribecoko")
        .insertAdjacentHTML(
          "beforebegin",
          `<p class="debrief">If you want to subscribe to the mailing list, please add your email.</p>`
        );
    } else if (document.querySelector(".debrief")) {
      document.querySelector("#subscribecoko").value = "false";
      document.querySelector(".debrief").remove();
    }
  });
