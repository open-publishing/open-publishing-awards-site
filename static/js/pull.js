let token;

let population = document.querySelector(".response-participants");
let eventsSpace = document.querySelector(".response-events");

document.querySelector("#submit").addEventListener('click', fetch);
let username, password;
function fetch() {
    username = document.querySelector("#username").value;
    password = document.querySelector("#password").value;
    login(username, password)
}




async function login(username, password) {

    axios
        .post(`https://openpublishingfest2.cloud68.co/auth/local`, {
            identifier: username,
            password: password,
        })
        .then(async function (response) {

            token = response.data.jwt;

            // document.querySelector(".form").remove();
            if (response) {
                await loadnominee();



                document.querySelector(".form").remove();
                if (document.querySelector('.login-error')) {
                    document.querySelector('.login-error').remove();
                }
            }
        })
        .catch(function (error) {
            console.log('there is an error:', error)
            document.querySelector(`.login-error`).innerHTML = `<p class="error">Sorry mate can’t connect, forgot you pass again? Then you need to contact your administrator.</p>`;
        });
}




async function loadnominee() {

    axios
        .get(`https://openpublishingfest2.cloud68.co/opa-nominees/?_limit=-1`, {

            headers: {
                Authorization: `Bearer ${token}`,
            },
        })


        .then(await function (response) {
            let data = response.data;
            let table = document.querySelector('.nominees tbody')
            response.data.forEach(nominee => {
                console.log(`nominee`, nominee)
                let row = document.createElement('tr');
                if(nominee.AcceptedForKotahi != true) {
                    row.classList.add('refused')
                }
                row.innerHTML = `
                <td>${nominee.id}</td>
                <td>${nominee.category}</td>
                <td>${nominee.projectName}</td>
                <td>${nominee.projectUrl}</td>
                <td>${nominee.description}</td>
                <td>${nominee.special}</td>
                <td>${nominee.AcceptedForKotahi}</td>`


                table.insertAdjacentElement('beforeend', row)

            });
        })
}




document.querySelector("#show").addEventListener('click', function() {
    this.innerHTML = 'hide';  
    document.body.classList.toggle('showAll');
})

