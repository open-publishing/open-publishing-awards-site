// markdown parser
const slugify = require("slugify");
let markdownIt = require("markdown-it");
let implicitFigures = require("markdown-it-implicit-figures");
let blockEmbedPlugin = require("markdown-it-block-embed");
let html5Embed = require("markdown-it-html5-embed");
let frame = require("markdown-it-iframe");
const fg = require('fast-glob');


module.exports = function(eleventyConfig) {
// commms collection

// Run search for images in /images/comms/ and /sponsors
const comImages = fg.sync(['**/images/comms/*', '!**/temp', '!**/public',]);

//Create collection of gallery images
eleventyConfig.addCollection('commsImages', function(collection) {
  return comImages;
});

// add a replace function
eleventyConfig.addFilter('replace', function(value, replace, replaceby) {
  return value = value.replace(replace,replaceby);
})

// filter with years metadata !
  eleventyConfig.addFilter('filterYearMD', function(collection, year) {
    if (!year) return collection;
    
      const filtered = collection.filter(item =>{ 
        return item.data.year.includes(year)
      })
      return filtered;
  });


  eleventyConfig.addFilter("slugify", function (str) {
    return slugify(str, {
      lower: true,
      replacement: "-",
      remove: /[*+~.·,()'"`´%!?¿:@]/g
    });
  });

//  create a collection to be able to call the why
  eleventyConfig.addCollection("why", function(collectionApi) {
    return collectionApi.getFilteredByTag("why");
  });
  // collection for recipient
  eleventyConfig.addCollection("recipient", function(collectionApi) {
    return collectionApi.getFilteredByGlob("./views/recipients/**/*.md");
  });
  // collection for judges
  eleventyConfig.addCollection("judges", function(collectionApi) {
    return collectionApi.getFilteredByGlob("./views/judges/**/*.md");
  });


  // markdown

  let markdownLib = markdownIt({ html: true })
    .use(implicitFigures, {
      dataType: false, // <figure data-type="image">, default: false
      figcaption: false, // <figcaption>alternative text</figcaption>, default: false
      tabindex: true, // <figure tabindex="1+n">..., default: false
      link: false, // <a href="img.png"><img src="img.png"></a>, default: false
    })
    .use(frame)
    .use(blockEmbedPlugin, {
      containerClassName: "video-embed",
      outputPlayerSize: false,
    });

  eleventyConfig.setLibrary("md", markdownLib);

  eleventyConfig.addFilter("markdownify", (value) => markdownLib.render(value));

  eleventyConfig.setTemplateFormats(["njk", "md"]);

  return {
    dir: {
      input: "views",
      output: "temp",
    },
    passthroughFileCopy: true,
  };
};
