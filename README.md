# open publishing awards website


Home page options:

- nominations are open: 
    → hero block
    → welcome block
    → nomination form
    → awards categories
    → sponsors

- nominations are close and nominees are known:
    → hero block
    → welcome block
    → nomination are closed 
    → award categories (are they needed?)
    → sponsors

-  nominations are closed and awards are known
    → welcome + awardees 
    → List of attendees
    → link to about page