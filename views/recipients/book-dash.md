---
category: Open Content
title: Book Dash
url: https://bookdash.org/
intro: Book Dash is a not-for-profit, social-impact publisher of South African picture books for young children. Their vision is for every child in South Africa to own one hundred books by the age of five. The judges were simply blown away with the publishing model and how this project leverages open to achieve such an enormous impact with very few resources.
twitter: bookdash
year: ['2019']
layout: recipient.njk
---

Book Dash is an astonishing project with openness drilled into everything it does. It has achieved enormous positive social impact through leveraging ‘open’ and collaboration in ways that should inspire us all.

The judges were simply blown away with how this project leverages open to achieve an enormous impact with very few resources.

Book Dash is a not-for-profit, social impact publisher of South African picture books for young children. Their vision is for every child in South Africa to own one hundred books by the age of five. Book Dash focuses on three core activities:

1. creating beautiful, fun, new, African picture books using the innovative “Book Dash” 12 hour publishing methodology.

2. translating the Book Dash book into the official languages of South Africa and making them available online for anyone to read, translate and distribute.

3. sourcing funding to print these books cost-effectively and distribute them freely and widely across the country to young children and their families who could never otherwise afford to own books. 

South Africa is one of the most unequal societies in the world (as measured by the Gini Coefficient) and faces a literacy crisis. To address this crisis, the country requires increased access to reading resources at very young ages, because in early childhood there is unparalleled cognitive, physical and emotional development in a child. But books are a luxury for the vast majority of South African children.

The Book Dash Model dramatically reduces the costs involved in publishing and printing these much needed books. Book Dash books are all made in 12 hour events ( a ‘book dash’) by volunteer creatives. Because it is an all volunteer effort and the books are published under an open license, the only remaining cost to print hard-copy children’s books is the printing cost itself. This means Book Dash can produce high quality children’s books very cost effectively.

Book Dash is defined by its rich approach to openness:

- the books are published under an open license (Creative Commons Attribution 4.0).
- the sources files are available under the same open license (on the website for anyone to access and use).
- the exciting and innovative content creation model – the Book Dash – is open (the 12-hour Book Dash events have been replicated in Nigeria, Angola, Laos, Cambodia and France).

As a result of this approach in the five years since their inception, Book Dash has:

- produced 128 original South African children’s books and published them under a Creative Commons license.
- translated those into a library of over 450 versions across all 11 official languages.
- printed and distributed over 650 000 physical copies of those books freely to children across South Africa, and – reached millions more readers through their website, app, and other digital redistribution platforms. 

In addition to these, the power of open extends the reach of these books exponentially: it enables the books to be read by people in places that would never have been reached with a more traditional approach to copyright. Anyone, anywhere, can adapt, translate, animate, download, print, distribute and even sell the books, because the open license imposes no restrictions.

Consequently, Book Dash books are published on 50+ reading platforms and apps. These platforms like the quality and the ease of use of the Book Dash books, and the readers love the fact that the books show a diverse society where children can recognise themselves in the pages, and have fun. These websites are truly international; readers from India, South America, the UK, Japan, etc contact Book Dash to say that they love reading the books.

Many communities also do their own translations of the books. Book Dash publishes versions the books in South African languages. These versions are often created by the Nal’ibali’s team of language practitioners. However the open license also allows language communities across the globe that love the books to do their own translations, thereby adding to the available reading resources in their language. Some of the more interesting examples are:

- A South African living in Norway was disappointed by the homogeneously white characters she encountered in Norwegian children’s books. So she translated and printed some Book Dash books to Norwegian.
- The organisation Gahwara translates books into Farsi, Pashto and Uzbek. Some Book Dash books are available in these languages, and are being read and loved by kids in Afghanistan.
- A group of language activists in Italy who are rekindling the minority Lombard language has translated some Book Dash books into Lombard.

Book Dash books can be and are also adapted for other media including youtube channel content, audio books, and in one case books have been versioned into sign language.

These are just a few examples of how the power of open allows this remarkable project to create beautiful children’s books that travel without boundaries, bringing joy to more and more parents and children all over the world.