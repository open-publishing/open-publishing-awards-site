---
category: Open Source Software
title: Stencila
url: https://stenci.la/
intro: "The Open Source Software award goes to Stencila - a powerful web platform which provides researchers with a set of tools that enable seamless collaboration.


Stencila bridges the gap between reproducible documents and legacy formats such as Word documents. Stencila Open is built on web standards, and improves machine-readability and accessibility of articles.
This software is designed for researchers. It allows the authoring of interactive, data-driven publications in visual interfaces, similar to those in conventional office suites, but is built from the ground up for reproducibility.
By using Stencila Open, a researcher can share a link to a preview their work, and their collaborator could view the link. They can also download a reproducible Word document or Google Doc to review and edit. This way, Stencila allows open and transparent research to be shared more effectively between colleagues of varying workflows and technical skill levels.


The jury finds this project impressive as it empowers open research and open collaboration, by making publications easier and more accessible. Kudos to the team behind the project for their work and dedication!"
twitter: stencila
year: ['2021']
layout: recipient.njk

---

