---
category: Open Content
title: "Phone and Spear: A Yuta Anthropology"
url: https://miyarrkamedia.com/bauman_portfolio/phone-spear-a-yuta-anthropology/
intro: "Coming from Australia *Phone & Spear : A Yuta Anthropology* is an astonishing document about how different worlds come together and produce something entirely new and exciting. Yuta means ‘new’ and so this is a document of new anthropologies. In these indigenous Australian stories, Phone and Spear looks closely at how mobile phones are used to create images - Yolngu social aesthetics - to remember and connect with people and the land.The images and stories are beautiful and they challenge us to think about mobile phones as a vehicle for possible deeper connections.
 

One of our judges said - “An excellent, visual and thought provoking project which invites us to stop and think about something that most of us take for granted, shaping us in ways we cannot imagine.”


We also very much appreciate the comments made by the project that illustrate that A Yuta Anthropology is not fully open content and deliberately so. The project highlights the difficulties in translating 'open' as it has been characterised by the various open movements when it comes to documenting these stories."
twitter: 
year: ['2021']
layout: recipient.njk

---

