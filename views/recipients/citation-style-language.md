---
category: Open Source Software
title: Citation Style Language
url: https://citationstyles.org/
intro: Citation Style Language (CSL) is simply a very important project for publishing. It has widespread adoption in important platforms and plays a critical role in the scholarly publishing landscape. It is also important in that it is an open source project populated by a diverse set of skill sets and research perspectives. The judges felt that in recent discussions about publishing infrastructure important projects like CSL are often under discussed because they are so foundational to be rendered almost invisible. We would like to celebrate and highlight the achievements of this very important project.
twitter: csl_styles
year: ['2019']
layout: recipient.njk

---

Citation Style Language (CSL) is simply a very important project for publishing. It has widespread adoption in important platforms and plays a critical role in the scholarly publishing landscape. It is also important as an open source project populated by a diverse set of skill sets and research perspectives.

The judges felt that in recent discussions about publishing infrastructure, important projects like CSL are often inadequately discussed because they are so foundational as to be rendered almost invisible. The judges want to celebrate and highlight the achievements of this very important project.

The Citation Style Language project developed an XML-based format to define citation formats. Originally it was built to support the OpenOffice platform, but has since been adopted on a wide scale.

The projects maintains a free and open source repository with currently over 9000 CSL citation styles for major style guides and individual journals (see https://github.com/citation-style-language/styles/ and https://pinux.info/csls_counter/). Dozens of software products, including popular reference managers such as Zotero, Mendeley, and Papers, have adopted CSL and its style library to give their users the ability to easily generate citations in a large variety of citation formats (a full list of known adopters is available on the https://citationstyles.org/ frontpage).

CSL is a special open source project in several aspects. First, its developers and maintainers have predominantly been people with a background and/or career in academia who have build this in their spare time. Secondly, a lot of the value of the project is in the curation of its style library, whereas most open source resources offer more developer-oriented products such as a specification or a software tool/library.

The CSL has had many outside contributions, many from individual researchers who wish to contribute a citation style for their journal of interest. https://github.com/citation-style-language/styles/ has over 2300 forks and over 700 unique contributors.

In the world of open infrastructure and the importance of ‘invisible but foundational layers’, there aren’t many examples of the same caliber as CSL. 