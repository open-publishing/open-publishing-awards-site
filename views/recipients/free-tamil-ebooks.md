---
category: Open Content
title: Free Tamil Ebooks
url: https://freetamilebooks.com/
intro: The Free Tamil Ebooks (FTE) website has existed for 5 years and releases openly licensed Ebooks in the Tamil Language (spoken in Tamilnadu, India). The judges were impressed by the very pragmatic approach Free Tamil Ebooks took to address a very real need. The project also provides educational material on open licenses, explains how to approach bloggers to re-license content as Creative Commons, and provides information on how to use free software tools (particuarly PressBooks) to make Ebooks. Free Tamil EBooks is a comprehensive ‘ecosystem’ approach to enabling the production of free content available in Tamil.
twitter: freeTamilEbooks
year: ['2019']
layout: recipient.njk

---

You can find the full team listed here [http://freetamilebooks.com/meet-the-team/]()

Contact the team here [kaniyamfoundation@gmail.com](mailto:kaniyamfoundation@gmail.com) (also for donations).

The judges were impressed by the very pragmatic approach Free Tamil Ebooks took to address a very real need. The project also provides educational material about open licenses, explains how to approach bloggers to re-license content as Creative Commons, and provides information on how to use free software tools (particuarly PressBooks) to make ebooks. Free Tamil EBooks is a comprehensive ‘ecosystem’ approach to enabling the production of free content available in Tamil.

The Free Tamil Ebooks (FTE) website has existed for 5 years and releases openly licensed Ebooks in the Tamil Language (spoken in Tamilnadu, India). The books are released in epub, mobi (Kindle format), A4 PDF, and 6 inch PDF so that they can be read on any mobile device such as smart phones, tablets, kindles, laptops, desktops etc.

Over the five years the project has released 550 free Ebooks with over 7,000,000 downloads. All the works are created by volunteers of the amazing Kaniyam Foundation. All the production work including collecting content from authors, making cover images, ebook making, releasing and promotions is done by committed volunteers.

The project started when founder T.Shrinivasan (Shrini) bought an Amazon Kindle and discovered there were no Ebooks available in the Tamil language. There was no support for Tamil fonts either at that time. Shrini, being an open source developer, found ways to make PDF files (with embedded fonts) so he could read Tamil content on his kindle. The files also had to be formatted as 6 inch (9cm x 12cm) PDF files as the A4 format could not be read on a Kindle. The conversions were done using only free software (notably PressBooks). At the same time as he was doing this Shrini discovered that many other Tamil people had the same problem. Shrini, decided to share the PDF books along with epub and mobi formats, so that any one could read the books on any device. Shrini then launched the website which now helps millions of readers and hundreds of authors.

What is also interesting is that the Free Tamil Website also educates its readers about Creative Commons licenses, how to re-license content, advice on approaching bloggers for permission to re-license content, and how to make Ebooks with free software.

Free Tamil Ebooks encourages readers to write to bloggers to get permission to re-license blog content as Creative Commons so that the articles can be bundled as free Ebooks for wider distribution among the Tamil community. The project goes as far as providing an email template to send to bloggers requesting this permission and a list of blogs that have already given their permission. The site also provides great advice on etiquette when doing this such as:

> What if an author refuses your request ? – We must leave them and their works intact. We must leave them alone and continue our efforts towards successive writers. 

Which speaks volumes to the respect this project shows, well, people.

The website also helps train people to use PressBooks to make eBooks (https://www.youtube.com/watch?v=bXNBwGUDhRs) and how to make PDF from Ms Word files http://freetamilebooks.com/create-pdf-files-using-microsoft-word/

Because, all the Ebooks are “Open” with creative commons, which allows sharing, all the books are shared in many platforms, mailing lists, telegram channels and other social media. Offline digital libraries keep a copy of all the books for localized sharing. By giving the Ebooks away free, DRM free and readable on any device Tamil readers get more content and Tamil authors reach more readers.

We love how Free Tamil Ebooks started off as one person addressing their own need, and through the power of openness this has grown to benefit thousands of people.