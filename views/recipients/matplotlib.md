---
category: Open Source Software
title: Matplotlib
url: https://matplotlib.org/
intro: matplotlib is versatile and powerful Python data visualization library. It has been recently used for rendering the first picture of a black hole and to illustrate the existence of gravitational waves. As with so many open source software projects, the judges were impressed not by the newness of the project, but how critical matplotlib is to the current and future open publishing landscape. If we did not have matplotlib we would simply have to invent it.
twitter: matplotlib
year: ['2019']
layout: recipient.njk

---

matplotlib is versatile and powerful Python data visualization library. It has recently been used for rendering the first picture of a black hole and to illustrate the existence of gravitational waves.

matplotlib is a Python 2D plotting library which produces publication quality figures in a variety of hardcopy formats and interactive environments across platforms. matplotlib can been be used in Python scripts, the Python and IPython shells, and has been implemented in Jupyter notebooks, web application servers, and four graphical user interface toolkits.

matplotlib is part of these projects that many use without really thinking about it. When you’re developing in Python, it is natural to use matplotlib to quickly plot your results. But there was a time (before matplotlib) where such plotting was not obvious at all. matplotlib is now so common that we tend to forget there are a lot of people working to support this library and those people deserve recognition. Its use might be so prolific you could be using it today without knowing you are doing so.

Once again, as with so many open source software projects, the judges were impressed not by the newness of the project, but how critical matplotlib is to the current and future open publishing landscape. If we did not have matplotlib, we would simply have to invent it — recognisably no simple task.

matplotlib was founded by the late John Hunter in 2003 and was open source from the start. This choice helped to build a large community of users whose feedback helped to improve and disseminate the library.