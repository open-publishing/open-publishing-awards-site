---
category: Open Publishing Lifetime Contribution
title: Creative Commons
url: https://creativecommons.org/
intro: " This year turning 20, Creative Commons is established as a central, universally respected non-profit organization that creates and manages Open licenses for digital artifacts. While a big barrier to lifelong learning can be the cost and access to resources, and initiatives such as Open Access aim to change this, it remains important for people and organizations to know how to use their resources legally. The CC project have been crucial for us to overcome legal obstacles to the sharing of knowledge and creativity.


Creative Commons licenses and its public domain tools give every person and organization in the world a free, simple, and standardized way to grant copyright permissions for creative and academic works; ensure proper attribution; and allow others to copy, distribute, and make use of those works.


The impact of CC in open access over the years has been enormous, so we’d like to celebrate this project today, and express our gratitude for the impact it has brought in the past decades."  
twitter: creativecommons
year: ['2021']
layout: recipient.njk

---

