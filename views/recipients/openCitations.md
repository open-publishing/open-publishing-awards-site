---
category: Open Data
title: Open Citations
url: http://opencitations.net/
intro: "With so few semantic web and linked open data initiatives
available in the scholarly communication landscape we wanted to present this award to a project that represents the open
principles so well, so effectively and so adamantly.


For OpenCitations, “open” is the crucial value and the final purpose.
OpenCitations fully espouses the UNESCO founding principles of Open
Science. It complies with the FAIR data principles proposed by Force11
that data should be Findable, Accessible, Interoperable and Re-usable,
and with the recommendations of I4OC that citation data in particular
should be Structured, Separable, and Open. By connecting publications
documenting our individual scientific and cultural achievements,
citation data provide the primary record of the growth of scholarly
knowledge, and an explanation for how we know facts. 
OpenCitations consider the free availability of bibliographic citation
data a necessary condition for the establishment of an open knowledge
graph. Having citations “open” helps achieve a more transparent,
accessible and comprehensive research practice. OpenCitations believes in the power of the scholarly community to change previous practice and, by
reclaiming ownership of its own data, to create an open and inclusive
future for science and research.


I would like to highlight a significant comment given on behalf of the jury panel. “At the time of writing this review, the largest database provided by
OpenCitations contains more than 1.23 billion citations. Compiling this
database in a license-friendly way is a feat on its own, but combine
that with OpenCitations’ persistence (established 11 years ago), their
active and consistent involvement with the community, and the number of
works that were made possible by their effort (Google Scholar lists 1440
results), it is clear that OpenCitations is one of the fundamental
projects in open publishing, specifically in open scientific publishing”."
twitter: opencitations
year: ['2021']
layout: recipient.njk

---