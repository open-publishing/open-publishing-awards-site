---
category: Open Content
title: "Standard ebooks"
url: https://standardebooks.org/
intro: "First up in our Open Content Awards is what one of our judges called “an example of an open source, community volunteer effort to help anyone, anywhere get the books they wish to read on the platform of their choice and drawing from those books that are in the public domain”.
   

This volunteer-driven, globally reaching project harnessing existing open content, effective and openly documented tools and standards to bring quality content from behind access barriers.
   
   
Standard Ebooks takes ebooks from sources like Project Gutenberg, formats and typesets them using a carefully designed and professional-grade style manual, fully proofreads and corrects them, adds an attractive public domain cover, and then builds them to create a new edition that takes advantage of state-of-the-art ereader and browser technology. Hundreds of contributors from all over the world work together to produce these ebooks and release them for free. The ebooks themselves are available for free download for various platforms on our website. Additionally, the source code of our editions is hosted on GitHub so anyone can submit corrections or improvements, and inspect the complete change history of each ebook.


The style manual and the ebook production toolset are also openly available for public inspection and improvement at GitHub.


As a fellow open publisher, I truly appreciated the commitment to not only increasing accessibility of quality content, but the time and care it takes to also document and publish one’s tools, and standard such as the style guide -- this is such a valuable contribution to open publishing, and publishing in general which can be an heavily guarded, inaccesible world very much intentionally -- which in turn makes quality self-publishing fose those without experience in publishing themselves, almost impossible-- Standard ebooks have contributed valuably to this aspect of open publishing, as much as bringing a whole library of open content that much closer to their readers."
twitter: standardebooks
year: ['2021']
layout: recipient.njk

---

