---
category: Open Publishing Lifetime Contribution
title: Public Knowledge Project
url: https://pkp.sfu.ca/
intro: Since 1998, the Public Knowledge Project (PKP) has been developing open source software for scholarly publishing including Open Journal Systems (OJS) and Open Monograph Press (OMP). The project gives academic communities, including researchers, librarians, students, and staff, opportunities to develop scholarly capacities with a global reach. PKP has made a clear and important impact on the world of open publishing over the last 20+ years and has cut a path for many that have followed. The open publishing sector, and particularly those in scholarly communications, owe a debt of gratitude to this essential and pioneering project.
twitter: pkp
year: ['2019']
layout: recipient.njk
---

Since 1998, the Public Knowledge Project (PKP) has been developing open source software for scholarly publishing, including Open Journal Systems (OJS) and Open Monograph Press (OMP). The project gives academic communities, including researchers, librarians, students, and staff, opportunities to develop scholarly capacities with a global reach.

PKP supports this capacity in part by providing open source platforms to the scholarly publishing sector.

PKP is without doubt one of the most important open source projects in the Scholarly Communications sector. The judges wanted to acknowledge its clear and important contribution over the last 20 years.

In addition to software, over the years PKP has sought to ensure the following:

- Support for users. Especially for those who are relatively new to scholarly publishing and online platforms.
- Evidence-based practice. PKP’s research activities provide an informed basis for understanding the socio-economic, historical and global dimensions of scholarly publishing.
- Sustainable open access. Informed by its research activities and interactions with their user community, PKP serves to advance a sustainable future for universal open access.

PKP made the decision to go ‘open’ in what were the early days of ‘open source’ software, some two decades ago. While most Scholarly Communications projects did not know of open source, or didn’t understand it, it made perfect sense to PKP to use open source software to create a system designed to encourage and facilitate open access to research. The project’s unequivocal uptake of ‘open’ has likely inspired many of the contributions that its community of users continue to make to the software, knowing that what they do will be open to everyone.

PKP has made a clear and important impact on the world of open publishing over the last 20+ years and has cut a path for many that have followed. The open publishing sector, and particularly those in scholarly communications, owe a debt of gratitude to this essential and pioneering project.