---
category: Open Source Software
title: Recogito
url: https://recogito.pelagios.org/
intro: Recogito is a great example of an open source software project that considers ‘open’ as more than just a license. Recogito has a very open model for the ongoing development of the tool, actively involving its constituency in conversations about what they want to see in the tool. The judges felt that the tool itself is obviously impressive and has impact, but including the people that need to use the tool in the development process has undoubtedly lead to the tools successful uptake and adoption by researchers. It is an enlightened open source development process addressing a real use case.
twitter: pelagiosnetwork
year: ['2019']
layout: recipient.njk

---

Recogito is a great example of an open source software project that considers ‘open’ as more than just a license. Recogito has a very open model for the ongoing development of the tool, actively involving its constituency in conversations about what they want to see in the tool. As a result, the Recogito team have learned a great deal from researchers about how the tool is used and how to improve it.

The judges felt that the tool itself is obviously impressive and has impact, and that including the people that need to use the tool in the development process has undoubtedly led to the tool’s successful uptake and adoption by researchers. It is an excellent example of an enlightened open source development process addressing a real use case.

Recogito is a web-based environment for collaborative semantic annotation. It is open source software, and provides support for working with either text or image documents (including those served via IIIF, an open standard for publishing high-resolution imagery online). Originally, the tool was designed with a focus on scholarly geographic annotation, i.e. the transcription, marking up and geo-resolving of maps and geographical texts, such as itineraries and travel reports, in the context of historical scholarship (e.g. to create maps from text, or to prepare a digital scholarly edition).

Over time and in response to ways in which the tool was used and what researchers wanted, Recogito’s feature set has grown continuously to: provide more general annotation functionality; widen the scope for other potential application areas; and allow for easy publication of research outputs online.

Recogito stands out for its ease of use, the unique way in which it enables novice users to perform a fairly complex and technical task – namely to create semantic markup on texts or images – without the need to deal with any of the technical aspects of semantic web technology and terminology.

Recogito speaks to a real use case and has seen significant uptake by scholars, who have made use of it for a wide range of exciting (and sometimes even surprising) purposes. Some examples include:

1. the study and transcription of historical maps.
2. as a collaboration, annotation and sharing platform in image conservation.
3. to build place name databases from medieval manuscript sources.
4. to create online editions of historical travel accounts. 

Recogito has also been increasingly adopted in teaching. As a free, easy to use, and visually appealing platform, it is now being featured regularly in Digital Humanities schools, targeting researchers who want to improve their digital skills and explore new approaches for digital scholarship (e.g. recently in the Oxford Digital Humanities Summer School, the Linked Open Data Indian Summer School in Mainz, or classes taught, among other places, in Sofia, Cluj-Napoca, Ankara, Tbilisi, London and Athens).

Further to this, discussing and analyzing historical documents by annotating them in Recogito and producing related visualizations is currently used to assess students in at least three higher education institutions: the University of London, Boğaziçi University, Istanbul and King’s College, London.

On the one hand, Recogito is open source software, designed for collaborating on the annotation of documents in order to produce open (annotation) data. On the other hand, the project team have been aiming to establish an open culture and spirit. Openly soliciting and acting on feedback has been built into the way Recogito is being developed and updated. Because of this, they have been able to discover a great deal about how researchers are using the tool, and to what purposes. Being flexible and receptive to various needs (such as making more storage space available to scholars working on large images, or incorporating unexpected feature requests) has resulted in the team being able to build strong working relationships with scholars from a broad range of disciplines and regions – which continues to be an exciting and rewarding experience for all.