---
category: Open Publishing Models
title: Open Library of Humanities
url: https://www.openlibhums.org/
intro: The Open Library of Humanities is a born open-access publisher which specialises in internationally-leading, rigorous and peer-reviewed scholarship across the humanities disciplines in 27 journals. The platform is 100% open-access, but unlike every other major humanities OA journal publication platform, it has no fees for authors. OLH is instead supported by approximately 250 academic libraries worldwide. The Open Library of Humanities (OLH) is a force of nature. Uncompromisingly open and collaborative. The judges were impressed about every aspect of OLH but without doubt their carefully considered Open Access economic model is comprehensive and inspiring.
twitter: openlibhums
year: ['2019']
layout: recipient.njk

---

The Open Library of Humanities (OLH) is a force of nature: Uncompromisingly open and collaborative. The judges were impressed with every aspect of OLH, but without doubt, the OLH carefully considered Open Access economic model is comprehensive and inspiring.

The Open Library of Humanities is a born open access publisher which specialises in internationally leading, rigorous and peer-reviewed scholarship across the humanities disciplines in 27 journals. The platform is 100% open access, but unlike every other major humanities OA journal publication platform, it has no fees for authors. OLH is instead financially supported by approximately 250 academic libraries worldwide.

Working within a context in which funding councils worldwide have committed to the principle that outputs of publicly funded research “should be widely and freely accessible as soon as possible”, Martin Eve and Caroline Edwards theorised and then implemented a novel economic model for scholarly communication in disciplines where Article Processing Charges would neither work nor scale. This model is now operational and is changing the economic and behavioral patterns of university presses, libraries, journal editors, authors, and readers.

OLH has changed the practices of learned societies and journal editors, who have accordingly converted their journals to open access models.

OLH has changed the practices of authors and readers, disseminating research material for free, online.

As evidenced by the list of institutions participating, library purchasing behaviors have changed to spend a portion of their budgets on open access in the humanities through the OLH.

100% open access with appropriate open licensing, the OLH has also pioneered open access in culturally conservative contexts of the humanities disciplines with a novel underpinning economic model. This has had significant effects worldwide. For instance, the special collection on Postcolonial Perspectives in Game Studies examines the detrimental influence that Empire has had upon gaming. The editor of this collection noted, though, that:

> “When I sought a venue for the special issue on videogames and postcolonialism, I was thinking of the millions of people in the so-named global south countries who had no access to the articles in paywalled journals, where their own predicament was being discussed. Having taught in the UK and the USA before returning to India, the irony was all too obvious. OLH, therefore, was a godsend when the editors agreed to publish the special issue. Today, anyone with an Internet connection can access the articles for free.” 

Furthermore, the OLH currently has ongoing special collections building on the representation of Islam in the Western media; “Encounters between Asian and Western Art in the 20th and 21st centuries: a liberating influence for Asia?”; “Pride Revisited: Cinema, Activism and Re-Activation”; and “Right-Wing Populism and Mediated Activism: Creative Responses and Counter-Narratives”, among others.

In other words, the “openness” of the “Open Library of Humanities” is used to promote significant topics of global social justice, all from a peer-reviewed humanistic perspective.