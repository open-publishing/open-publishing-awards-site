---
category: Open Publishing Models
title: Zotero
url: https://www.zotero.org/
intro: "For this award we assessed on various parameters such as the works to be Open Source, Community led, versatile product and that can support collaboration in the notoriously walled
garden world of academia.



The award goes to a well-designed open-source solution for easy cross-platform
management of literature. It works with a variety of excellent
connectors that make it possible to comfortably collect a variety of
information sources beyond your classic journal article and can work
with metadata embedded in websites so as to semi-automatize adding new
sources to a bibliography.  This is why we thought Zotero deserves this award.



Zotero has an excellent implementation of metadata extraction to feed one's
bibliography - this effectly allows one to add elements with one or two
clicks, and bibliographic info becomes inserted into your Zotero
project. Also, when working with communities of scholars on a given
paper, the group library feature is extremely useful to enable the whole
group to collect references and sources that can then be fed into a paper."  
twitter: zotero
year: ['2021']
layout: recipient.njk

---

