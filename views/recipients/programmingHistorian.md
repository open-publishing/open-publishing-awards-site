---
category: Open Content
title: "Programming Historian"
url: https://programminghistorian.org/
intro: "Next Open Content Award goes to a very cool project coming out of Western University in Canada, born in 2008. It has since expanded to traverse four languages: English, Spanish, French and Portuguese, and thus reaching many geographic regions and users globally.


The project stems from a passion to encourage and enable researchers and educators in the humanities to learn a wide range of digital tools, techniques and approaches, making them better teachers and researchers.


Programming Historian is a multilingual open access journal that publishes novice-friendly, peer-reviewed tutorials for digital skills development.  The project now has published 147 lessons in 4 languages, reaching an annual audience of more than 1.6 million readers  around the world - all without charging anyone a penny.   

The project is committed to openness, tolerance, and diversity, constantly working to better meet the needs of a wide range of learners, living and working around the globe.


By adopting an open and inclusive approach, the Programming Historian has created a virtual community of enthusiastic people passionate about sharing their knowledge and building a better future. Amongst academic publications, it is unusual in its approach to sharing credit.


Openness is the cornerstone of everything we do in this project. That includes open access, open source, open review, and an open ethos to project planning.


Because all content is free (CC-BY), it has found an important niche in the Global South. The project’s global approach also means that editors consciously encourage authors to think carefully about readers’ needs from other cultural or linguistic backgrounds.


Authors are asked to assume that their reader does not come from their country, speak their language, or share their political or religious beliefs. This has meant that the resultant tutorials are culturally relevant to a wider number of potential readers, and with the readership growing five fold since 2016, now more than 1.6 million per year, it is clear that the appetite for knowledge of this kind continues to grow.


Programming Historian fills an important gap left by university teaching in the humanities, which makes it difficult if not impossible for scholars, teachers, and students to learn the digital skills that it will take to allow the field to grow in an increasingly connected world.


We love how this open publishing project considers not only the technical skill needs of those they serve, but also how to make the content they produce as high-quality and accessible to all as possible -- it shows true consideration of a global world and commitment to sharing across borders. A most deserved award!"
twitter: ProgHist
year: ['2021']
layout: recipient.njk

---

