---
category: Open Content
title: Upper Limb Anatomy Models
url: https://shhs.iupui.edu/about/directory/janson-robin.html
intro: Robin Janson, a Clinical Assistant Professor at Indiana University Department of Occupational Therapy set herself the task of making openly licensed bone models so that teachers and students around the world can make quality, low-cost, instructional models to enhance learning. This is the story of objects as open content, and how one woman educator is working passionately to make freely licensed bone models available to help her students. The story is also a great example of how open practitioners can build on the value and works created by other open practitioners.
twitter: 
year: ['2019']
layout: recipient.njk

---

This is the story of objects as open content, and how one woman educator is working passionately to make freely licensed bone models available to help her students. The story is also a great example of how open practitioners can build on the value and works created by other open practitioners.

https://shhs.iupui.edu/about/directory/janson-robin.html

Robin Janson, a Clinical Assistant Professor at Indiana University Department of Occupational Therapy, set herself the task of making openly licensed bone models so that teachers and students around the world could make quality, low-cost, instructional models to enhance their learning.

Prior to this work, a complete open source proportional upper limb skeleton was not available. Robin started this journey by first accessing already existing open licensed digitized 3D bones made available by others. Once Robin acquired the digital bone files from disparate sources, she spent many hours proportionally resizing the digital bone models (while referencing anatomical textbooks and skeletal models), and working out where to place the holes for enabling the articulation of joints in the 3D model.

It took many many hours and days, but when Robin had finished, she had a complete instructional model of an articulated upper limb (arm) skeleton ready to be printed. Robin published this mode under a Creative Commons license.

1. Upper Limb Bones (Left) Published on Thingiverse 2/18/2016 https://www.thingiverse.com/thing:1352085
2. Upper Limb Bone Anatomy Model (Left) published on Thingiverse 7/30/2018 with detailed instructions and lesson plan https://www.thingiverse.com/thing:3021328
3. Hand Bone Anatomy Model with Instructional Manual accepted by MyMiniFactory and published 12/18/2018: https://www.myminifactory.com/object/3d-print-hand-bone-anatomy-model-81620

To support experiential learning, students benefit from having ready access to instructional models they can manipulate during learning activities. Cost is an issue when purchasing instructional aids. With 3D printing, the material cost to print an entire upper limb bone model is $5.00; whereas commercially available upper limb anatomy models cost $50.00 or more.

Students in the Indiana University Occupational Therapy Program, where Robin is a teacher, have utilized the bone models as instructional aids during the Hand Rehabilitation Elective, Gross Anatomy, and Biomechanics courses.

This low cost has enabled students to 3D print models to support their learning needs. Importantly, students can also take the upper limb bone models home for study (which normally is not permitted with the more expensive commercially purchased bone models).

As an educator, Robin is thrilled to make this open contribution so that teachers and learners around the world can make a quality, low-cost, instructional model to enhance their learning!