---
category: Open Publishing Models
title: WikiJournals
url: https://en.wikiversity.org/wiki/WikiJournal_User_Group
intro: WikiJournals publishes a set of open-access, peer-reviewed academic journals with no publishing costs to authors. Its goal is to provide free, quality-assured knowledge. Secondly, it aims to bridge the Academia-Wikipedia gap by enabling expert contributions in the traditional academic publishing format to improve Wikipedia content. However it is the model that the judges found most interesting. WikiJournals play in the space between mass collaborative knowledge cultures like Wikipedia and more contained, linear models that we see in peer reviewed journals.
twitter: WikiJMed
year: ['2019']
layout: recipient.njk

---

WikiJournals publishes a set of open access, peer-reviewed academic journals with no publishing costs to authors. The goal is to provide free, quality-assured knowledge; and secondly, to bridge the Academia-Wikipedia gap by enabling expert contributions in the traditional academic publishing format to improve Wikipedia content.

The WikiJournals publishing group functions to shepherd articles through academic peer review for stable, citeable versions whose content can potentially benefit Wikipedia and other Wikimedia projects. The WikiJournals are created with the purpose of communicating important, high quality, reviewed, research to the world. They are intermediate between Wikipedia pages and Review articles in traditional peer-reviewed journals. For example a Wikipedia page can be taken and reviewed and edited to fit into a journal-like format. This is done without thought of formal “impact factor”, although ‘CV citation value’ is taken into count, but to take the “anyone edits” philosophy in Wikipedia to a consistent, definitive, reliable snapshot of knowledge produced through mass collaboration.

The most successful WikiJournal is perhaps the WikiJournal of Medicine and the content is quite impressive.

https://en.wikiversity.org/wiki/WikiJournal_of_Medicine

This is also the model that the judges found most interesting. WikiJournals play in the space between mass collaborative knowledge cultures, such as Wikipedia, and more contained, linear models seen in peer reviewed journals. It is an exploration of this space that is fascinating. In this space, continuous publishing and ‘canonical’ records are at tension, as is the interplay between named authors and a level of collaboration where authorship almost becomes invisible. The nature and status of content also comes into question, where an WikiJournal article might be a synthesis of various Wikipedia articles, or peer reviewed WikiJourmal articles might become part of the Wikipedia corpus. In this space traditional delineations become challenged.

WikiJournals could be the ultimate paradoxical conclusion of the battle between legacy post-paper publication models and the web, romantic notions of authorship, and networked collaboration. Whatever the case, it is provocative, fascinating, and quite successful on its own terms. Of course without open content, this model would not be possible. This, together with the use of open source software and an interesting model for open access publishing, makes WikiJournals a standout open project.