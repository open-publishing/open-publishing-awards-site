---
category: Open Publishing Models
title: AFRO-PWW
url: https://pww.afro.illinois.edu/
intro: AFRO-PWW presents a complete ecosystem approach to Open Access publishing. The project doesn’t just publish Open Access woks, it helps projects choose appropriate open source software for authoring/processing/publishing materials, plays an important role in training researchers to use these tools, and then educates their constituency as to Open Access licensing and publishing options. Further, AFRO-PWW is fully invested in exploring new digital models for scholarship and helps “scholars navigate the new opportunities presented by collaborative, multi-modal, and interim phase works. “ The judges felt that not only is AFRO-PWW interesting in itself but it is a strong model for filling important open knowledge gaps from marginalized communities. We hope the model is inspiring and useful to you and may influence open knowledge projects to come.
twitter: afropww
year: ['2019']
layout: recipient.njk

---

AFRO-PWW presents a complete ecosystem approach to Open Access publishing. The project doesn’t just publish Open Access works, it helps projects choose appropriate open source software for authoring/processing/publishing materials, plays an important role in training researchers to use these tools, and then educates their constituency as to Open Access licensing and publishing options. Further, AFRO-PWW is fully invested in exploring new digital models for scholarship and helps “scholars navigate the new opportunities presented by collaborative, multi-modal, and interim phase works. “

AFRO-PWW is a research and outreach initiative as well as an important new publication series setting a standard for new forms of open access digital publication in African-American Studies.

The judges felt that not only is AFRO-PWW interesting in itself but it is a strong model for filling important open knowledge gaps from marginalized communities. They hope the model is inspiring and useful to you and may influence open knowledge projects to come.

While individual AFRO-PWW titles could be considered in their own right, the AFRO-PWW series is a major endeavor to make new forms of open access digital publication more viable in the field of African-American Studies. While the rise of the digital humanities has led to the creation of a variety of different online resources, they remain an object of skepticism by some tenure and promotion committees due to a perceived lack of peer review and quality control. Additionally, web publications in the digital humanities have also been identified as re-inscribing biases toward white, male authors and limiting exposure to writers of color.

The AFRO-PWW project is developed in partnerships with Historically Black Colleges and Universities (HBCUs), and seeks to expand capacity for supporting high quality digital humanities publications not just about research in the area of African-American studies but by scholars at HBCUs. It thus expands work in a field that has been underrepresented in the digital humanities, and expands access to the means of production of digital publishing to a broader group of scholars.

Open access publishing plays a crucial role in making new forms of scholarship such as that supported by AFRO-PWW visible to a broader audience, exposure which is necessary to help normalize these types of publications. Because all AFRO-PWW projects are designed with not just the use of scholars but students and the interested public in mind, being open ensures accessibility for classroom use as well as members of the public interested in African-American Studies.