---
name: "Silva Arapi"
structure: "Cloud68.co"
structureURL: "https://cloud68.co/"
year: ['2021']
img: "silva-arapi.jpeg"
permalink: false
source: "https://www.openpublishingfest.org/attendees.html"
---
[Silva Arapi](https://www.linkedin.com/in/silva-arapi/) is an Online Privacy and Free Software Advocate. Both her professional and volunteer work consist in promoting technological innovation through open standards, open source software and information security. She is one of the co-founders of [Cloud68.co](https://cloud68.co/), working to build a sustainable company which makes the use of open source digital infrastructure easy and convenient.