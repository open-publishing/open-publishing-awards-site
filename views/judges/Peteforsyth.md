---
name: "Pete Forsyth"
structure: "Wiki Strategies"
structureURL: "https://wikistrategies.net/"
year: ['2021']
img: Pete_Forsyth_by_Christopher_Ellis.jpg
permalink: false
source: "https://www.linkedin.com/in/peteforsyth/"
---

[Pete Forsyth](https://www.linkedin.com/in/peteforsyth/) is an Internet and communications consultant with deep expertise in online peer production communities, specifically the production of open educational resources using wiki-based web sites like Wikipedia. He is the founder and principal of [Wiki Strategies](https://wikistrategies.net/) As the Wikimedia Foundation's first Public Outreach Officer, Forsyth worked with the vast international network of volunteer Wikipedia contributors. He was a key architect of the Wikipedia Public Policy Initiative, an innovative pilot project to support professors in using Wikipedia writing as a teaching tool.