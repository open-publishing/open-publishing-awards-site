---
name: "Violeta Ilik"
structure: "Adelphi University"
structureURL: "(https://www.adelphi.edu/faculty/profiles/profile.php?PID=0944)"
year: ['2021']
img: violettaIlik.jpg
permalink: false
source: "https://vioil.github.io/"
---

[Violeta Ilik](https://www.linkedin.com/in/violeta-ilik-46216a53/) is the Dean of [Adelphi University](https://www.adelphi.edu/) Libraries where she  is responsible for directing all activities of the institution’s libraries, which includes the Garden City, Manhattan, Hauppauge, and Hudson Valley libraries. She has been a long time member of many open source projects and non profit organizations that work to advance open initiatives. She served as Co-Chair of the Board of Directors of [FORCE11](https://www.force11.org/) (The Future of Research Communication and e-Scholarship), on the Steering Committee of FSCI (FORCE11 Scholarly Communications Institute), and served as a member of the Leadership and Steering Groups of the open source project [VIVO](https://vivo.lyrasis.org/). She currently serves on on the Board of Directors of [ConnectNY](http://connectny.org/).