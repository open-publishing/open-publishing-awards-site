---
name: "Omo Oaiya"
structure: "West and Central African Research and Education Network (WACREN)"
structureURL: "https://www.wacren.net/"
year: ['2021']
img: omoOaiya.jpg
permalink: false
source: "https://www.linkedin.com/in/omooaiya"
---

[Omo Oaiya](https://www.linkedin.com/in/omooaiya/) is the Chief Strategy Officer of the West and Central Research and Education Network [(WACREN)](https://www.wacren.net/). Omo facilitates WACREN strategy and business development planning and assists the CEO in implementation and monitoring oversight. Before WACREN, he was CEO of Datasphir, a private sector consultancy offering software development and project management support to the education sector in Nigeria and other parts of Africa. He currently leads LIBSENSE, a pan-African initiative to build the information management capability of librarians and researchers and strengthen local services to support open science and research in Africa."