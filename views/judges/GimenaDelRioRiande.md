---
name: "Gimena del Rio Riande "
structure: "CONICET"
structureURL: "https://www.conicet.gov.ar/?lan=en"
year: ['2019','2021']
img: Gimena-del-Rio.jpg
permalink: false
---

[Gimena del Rio Riande](https://www.linkedin.com/in/gimenadelrioriande0/) is Associate Researcher at the _Seminario de Edicion y Crítica Textual_ (SECRIT-IIBICRIT) and Director of Humanidades Digitales CAICYT Lab at the National Scientific and Technical Research Council ([CONICET](https://www.conicet.gov.ar/?lan=en), Argentina). She is co-Director of the Digital Humanities Master at [LINHD-UNED](http://linhd.uned.es/)(Madrid) and Professor at the Literary Studies Master’s degree at the University of Buenos Aires.   
With her MA and PhD in Romance Philology (Universidad Complutense de Madrid) with a critical edition of King Dinis of Portugal’s Songbook (_Texto y contexto: El Cancionero del rey Don Denis de Portugal: estudio filológico, edición crítica y anotación_. Summa cum Laude), Gimena’s main academic interests deal with Digital Scholarly Edition, Open Research, free technologies and the interaction of the global and the local in the Digital Humanities. She has been working since 2013 in building different DH communities of practice in Latin America and Spain, especially in Argentina, where she founded the DH association (AAHD). She is, among others, the co-founder of the first Spanish Digital Humanities journal, the [Revista de Humanidades Digitales](http://revistas.uned.es/index.php/RHD/index)(RHD), president of the [Asociación Argentina de Humanidades Digitales](http://aahd.com.ar/)(AAHD) and member of the Board of Directors of the TEI Consortium, FORCE11, and Pelagios Commons. She is vocal at Humanidades Digitales Hispánicas Association, and member of the board of editors at Hypothèses/Open Edition, Open Methods-DARIAH, Revista Relaciones (México), Bibliographica (México) and Digital Studies/Le Champ Numérique (Canada). 
