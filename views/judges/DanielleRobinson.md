---
name: "Danielle Robinson"
structure: "Code for Science & Society"
structureURL: "https://codeforscience.org"
year: ['2019']
img: danielle-robinson.jpg
permalink: false
---

Dr. [Danielle Robinson](https://www.linkedin.com/in/danielle-robinson-phd-694996b5/) is a scientist turned advocate for community-centered public interest technology. In 2016, she completed a PhD in Neuroscience at Oregon Health & Science University. During her PhD, she became increasingly interested in how the influence of funders, publishers, and other forces impact how research is conducted and communicated. This interest transformed her into an advocate for globally inclusive open access policies and launched her into fellowship with Mozilla Science. She’s been the President and Co-Executive Director of [Code for Science & Society](https://codeforscience.org), since 2017 where she focuses on bringing strategic perspective to projects looking to build, fund, and sustain open communities.
