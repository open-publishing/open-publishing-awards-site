---
name: "Neil Chue Hong"
structure: "Software Sustainability Institute"
structureURL: "https://www.software.ac.uk"
year: ['2019', '2021']
img: neil_headshot_crop.jpg
permalink: false
---

[Neil Chue Hong](https://www.linkedin.com/in/neilchuehong/) is the founding Director and Principal Investigator of the [Software Sustainability Institute](https://www.software.ac.uk), and is based at EPCC at the University of Edinburgh. He enables research software users and developers to drive the continued improvement and impact of research software and his current research is in barriers and incentives in research software ecosystems and the role of software as a research object. He is Editor-in-Chief of the Journal of Open Research Software.
