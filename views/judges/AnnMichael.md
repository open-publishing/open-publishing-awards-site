---
name: "Ann Michael"
structure: "PLOS"
structureURL: "https://www.plos.org/"
year: ['2019']
img: ann-michael-sq.jpg
permalink: false
---

[Ann Michael](https://www.linkedin.com/in/annmichael/) is Chief Digital Officer at [PLOS](https://www.plos.org/) charged with driving the development and execution of the organization’s overall digital and supporting data strategy. Working collaboratively across PLOS, and through industry collaboration, her team will facilitate the strategic evaluation and evolution of PLOS platforms and processes.  

Prior to joining PLOS, Ann was Founder and CEO of Delta Think. 