---
name: "Natasha Simons"
structure: "ARDC"
structureURL: "https://ardc.edu.au"
year: ['2019', '2021']
img: natachaSimons.jpg
permalink: false
---

[Natasha Simons](https://www.linkedin.com/in/natasha-simons-69814837/) is Associate Director, Skilled Workforce, for the [Australian Research Data Commons](https://ardc.edu.au) (formerly ANDS, RDS and Nectar). With a background in libraries, IT and eResearch, Natasha has a history of developing policy, technical infrastructure (with a focus on persistent identifiers) and skills to support research. She works with a variety of people and groups to improve data management skills, platforms, policies and practices. Based at The University of Queensland in Brisbane, Australia, Natasha is co-chair of the RDA Interest Group on Data Policy Standardisation and Implementation, Deputy Chair of the Australian ORCID Advisory Group and co-chair of the DataCite Community Engagement Steering Group.
