---
name: "Shun-Ling Chen"
structure: "Academia Sinica"
structureURL: "https://www.iias.sinica.edu.tw/"
year: ['2021']
img: "shungLingChen.jpeg"
permalink: false
source: "https://compcore.cornell.edu/countries/taiwan/"
---

[Shun-Ling Chen](https://www.iias.sinica.edu.tw/en/member_post/16) is an associate research professor and the associate director of Information Law Center at the Institutum Iurisprudentiae, [Academia Sinica](https://www.iias.sinica.edu.tw/) (Taipei, Taiwan). She received her S.J.D. from Harvard Law School in 2013. She works in fields where society, information technologies and the legal system intersect, including the allocation of resources related to intellectual property law. She spent years studying the development and enforcement of communal norms in online peer production communities, as well as how these communities negotiate externally with established institutions.