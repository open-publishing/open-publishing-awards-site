---
name: "Jure Triglav"
structure: 
structureURL: ""
year: ['2021']
img: jureTriglav.png
permalink: false
source: "https://www.linkedin.com/in/jure-triglav/"
---

[Jure Triglav](https://www.linkedin.com/in/jure-triglav/) is a software developer focused on collaboration, previously working with [PLOS](https://plos.org/) and [Academia.edu](https://www.academia.edu/). For 5 years he was also the lead developer at the [Collaborative Knowledge Foundation]( a Lead Developer in Collaborative Knowledge Foundation [(Coko)](https://coko.foundation/), where he developed PubSweet and led its community. Now he is focusing on WebXR at FRAME, a VR meetings project.