---
name: "Jennifer Gibson"
structure: "Dryad"
structureURL: "http://www.datadryad.org/"
year: ['2021']
img: jennifer-gibson.png
permalink: false
source: "https://blog.datadryad.org/2021/08/17/dryad-appoints-jennifer-gibson-as-executive-director/"
---

[Jennifer Gibson](https://www.linkedin.com/in/jmclenna) is the Executive Director of [Dryad](http://www.datadryad.org/). Jennifer is also the Chair of Board of Directors for the Open Access Scholarly Publishing Association (OASPA). She is a former member of the board for FORCE11 (2018-2020). She has 15 years’ experience, driving openness in research through advocacy and leadership. She has worked with scientists, funders, publishers, libraries, developers and others to explore fresh paths toward accelerating discovery through open research communication and open-technology innovation. She is known for leading teams, designing strategies, and engaging communities of researchers, funders, institutions and publishers. She has beena principal part of esteemed organisations like eLife Science Publications Limited, and SPARC.