---
name: "Julia Norrish"
structure: Book Dash
structureURL: "https://bookdash.org/"
year: ['2021']
img: juliaNorrish.jpg
permalink: false
source: "https://www.linkedin.com/in/julia-norrish/"
---

[Julia Norrish](https://www.linkedin.com/in/julia-norrish/) is a book-lover and publisher passionate about increasing access to the joys and benefits of reading to all, especially the young children. She is the Executive Director at [Book Dash](https://bookdash.org/), a South African publisher of open, African picturebooks and a previous Open Publishing Awards winner for Open Content. Before Book Dash, Julia was involved in other open-licensed publishing projects in the [healthcare](https://bettercare.co.za/) and literacy fields, and education-based outreach projects in impoverished communities. 