---
name: "Cameron Neylon (Chair)"
structure: "Curtin University"
structureURL: "https://www.curtin.edu.au"
year: ['2019', '2021']
img: cameron-neylon.png
permalink: false
---

[Cameron Neylon](https://www.linkedin.com/in/cameronneylon/) is Professor of Research Communication at the Center for Culture and Technology at [Curtin University](https://www.curtin.edu.au/). He is interested in how to make the internet more effective as a tool for scholarship. He writes and speaks regularly on scholarly communication, the design of web based tools for research, and the need for policy and cultural change within and around the research community.  
Cameron Neylon is a one-time biomedical scientist who has moved into the humanities via Open Access and Open Data advocacy. His research and broader work focus on how we can make the institutions that support research sustainable and fit for purpose for the 21st century and how the advent of new communications technology is a help (and in some cases a hindrance) for this.
