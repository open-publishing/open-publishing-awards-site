---
name: "Anasuya Sengupta"
structure: "Whose Knowledge?"
structureURL: "https://www.whoseknowledge.org"
year: ['2019']
img: anasuya.jpg
permalink: false
---

[Anasuya Sengupta](http://sanmathi.org/anasuya/) is Co-Director and Co-founder of [Whose Knowledge?](https://www.whoseknowledge.org). She has led initiatives in India and the USA, across the global South, and internationally for over 20 years, to amplify marginalized voices in virtual and physical worlds. She is the former Chief Grantmaking Officer at the Wikimedia Foundation, and a 2017 Shuttleworth Foundation Fellow. She received a 2018 Internet and Society award from the Oxford Internet Institute. Anasuya holds an MPhil in Development Studies from the University of Oxford, where she studied as a Rhodes Scholar. She also has a BA in Economics (Honors) from Delhi University. When not rabble-rousing online, Anasuya builds and breaks pots, takes long walks by the ocean and in the redwoods, and contorts herself into yoga poses.
