---
name: "Adele Vrana"
structure: "Whose Knowledge?"
structureURL: "https://www.whoseknowledge.org"
year: ['2019']
img: adele.jpg
permalink: false
---

[Adele Vrana](https://www.linkedin.com/in/adele-vrana-92572257/) is Co-Director and co-founder of [Whose Knowledge?](https://www.whoseknowledge.org). Adele has led business development and partnerships initiatives to help build more plural and diverse communities in her native country of Brazil and globally. She is the former Director of Strategic Partnerships at the Wikimedia Foundation, and a 2015 Erasmus Prize laureate on behalf of her work to expand access to Wikipedia in the Global South. Adele holds a BA in International Relations and a Master’s Degree in Political Science from the University of Sao Paulo. When not re-imagining what the internet of the future would look like and advocating for that online, Adele spends most of her time raising two feminist boys, reading black feminists from the Global South, and spending time with her friends from close and afar. 
