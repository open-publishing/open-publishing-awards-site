---
name: "Siko Bouterse"
structure: "Whose Knowledge?"
structureURL: "https://www.whoseknowledge.org"
year: ['2019']
img: siko.jpeg
permalink: false
---
 
[Siko Bouterse](https://www.linkedin.com/in/sikobouterse/) is Co-Director and co-founder of [Whose Knowledge?](https://www.whoseknowledge.org). She’s organized, localized and imagined a more plural and truly global web for over 10 years. She is former Director of Community Resources at the Wikimedia Foundation. Siko has an MA in Middle East History from the American University in Cairo, where her award-winning thesis focused on social history not captured in traditional historical sources. She also holds a BA in Anthropology from UC Berkeley where she worked at the Phoebe Hearst Museum. When not rabble-rousing online, Siko is paddleboarding in the ocean, cooking and reading about delicious feasts, making bad mixed-media art, and raising a feminist daughter.
