---
name: "Brianna Laugher"
structure: "Planet Innovation"
structureURL: "https://planetinnovation.com/"
year: ['2021']
img: briannaLaugher.jpg
permalink: false
source: "http://brianna.laugher.id.au/"
---
[Brianna Laugher](https://www.linkedin.com/in/briannalaugher/) has been heavily involved in open source and related communities since 2007. She has worked on a Tagalog-English machine translation engine in Prolog and an automated weather forecast generation program in Python. Brianna is a software test lead at [Planet Innovation](https://planetinnovation.com/) and an organiser at [PyLadies Melbourne](https://github.com/pyladiesmelbourne). She has spoken at various events ranging from local user groups and workshops to international conferences. She is passionate about solving the problems of real users and bridging the communication gap between domain experts and developers. She is a core contributor to the [pytest library](https://docs.pytest.org/).