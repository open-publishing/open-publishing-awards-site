---
name: "John Chodacki "
structure: "California Digital Library"
structureURL: "https://www.cdlib.org/services/uc3/"
year: ['2019', '2021']
img: john_chodacki.jpg
permalink: false
---

[John Chodacki](https://www.linkedin.com/in/johnchodacki/) is Director of the University of [California Curation Center](https://www.cdlib.org/services/uc3/) (UC3) at [California Digital Library](https://www.cdlib.org) (CDL). As Director of UC3, John works across the UC campuses and the broader community to ensure that CDL’s digital curation services meet the emerging needs of the scholarly community, including digital preservation, data management, and reuse.  In addition, John represents CDL in the global research community (funders, libraries, archives, publishers, researchers) and defines and prioritizes new and improved services for UC3.  Prior to CDL, John worked at Public Library of Science (PLOS) where, as Product Director, he was responsible for product strategy and led key organization-wide initiatives in taxonomy development,  data policy, and article-level metrics. John serves on the FORCE11 Board of Directors, the DataCite Board of Directors and the Coko Advisory Board.
