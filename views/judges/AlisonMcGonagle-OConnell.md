---
name: "Alison McGonagle-O'Connell"
structure: "HighWire Press"
structureURL: "https://www.highwirepress.com/"
year: ['2021']
img: alisonMcGonagleOConnell.jpg
permalink: false
source: https://www.highwirepress.com/news/alison-mcgonagle-oconnell-joins-highwire-as-senior-director-of-marketing/
---

[Alison McGonagle-O’Connell](https://www.linkedin.com/in/alisonmcgonagle/) is Senior Director of Marketing at HighWire Press. 

O’Connell’s nearly 20 years of experience in scholarly communications include 10 years working for commercial publishers, and more recently, leading marketing and community initiatives for software as a service providers in the space including Ebsco Information Systems, Aries Systems, and Coko, where she was building the community around Editoria. She’s an active participant in industry initiatives and professional development organizations including the NISO CRediT initiative, Council of Science Editors, Peer Review Week, and more. 


