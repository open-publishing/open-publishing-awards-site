---
name: "Kat Walsh"
structure: "MindSpillage"
structureURL: "http://www.mindspillage.org/"
year: ['2021']
img: "katWalsh.jpg"
permalink: false
source: "https://www.linkedin.com/in/mindspillage//"
---
[Kat Walsh](https://www.linkedin.com/in/mindspillage/) is a technology, copyright, IP, and internet policy lawyer with the goal of building and maintaining legal infrastructure for social good. Kat is the co-author of version 4.0 of the CC license suite, author of Blockstream's patent pledge, and former chair of the Wikimedia Foundation. Her primary aim is to create a world where everyone can be a creator, a learner, a scholar, and a builder.