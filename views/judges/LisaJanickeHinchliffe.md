---
name: "Lisa Janicke Hinchliffe"
structure: University Library at the University of Illinois
structureURL: "https://illinois.edu/"
year: ['2021']
img: LisaHinchliffe.webp
permalink: false
source: "https://lisahinchliffe.com/"
---
[Lisa Janicke Hinchliffe](https://lisahinchliffe.com/) is Professor/Coordinator for Information Literacy Services and Instruction in the University Library at the University of Illinois at Urbana-Champaign. She is also an affiliate faculty member in the University’s School of Information Sciences. At Illinois, she has also served as Acting Head of the University High School Library, Head of the Undergraduate Library, Acting Coordinator for Staff Development and Training, and Coordinator for Strategic Planning in the University Library. Previously, she was the Library Instruction Coordinator at Illinois State University and Reference Librarian at Parkland Community College. 