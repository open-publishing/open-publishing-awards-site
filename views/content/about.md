---
layout: "about.njk"
title: About Open Publishing Awards
class: about
permalink: /about.html
---

## About the Open Publishing Awards

Open Publishing Awards celebrates individuals and communities developing open creative, technological, scholarly, artistic, and civic publishing projects.

As we witness a rise in the creation of content, software, systems, and infrastructure that help share stories, art, and knowledge globally without barriers, the Open Publishing Awards seek to recognize the contributions of individuals and organizations that advance open in publishing.

A moment for reflection and chance for celebration, the Open Publishing Awards bring together people, projects, and organizations from around the world to shine a light on one another.
