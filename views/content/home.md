---
layout: "home.njk"
title: Open Publishing Awards 2021
class: home
permalink: /index.html
year: "2021"
---

# Welcome to the Open Publishing Awards

Open Publishing Awards celebrates individuals and communities developing open creative, technological, scholarly, artistic, and civic publishing projects.

As we witness a rise in the creation of content, software, systems, and infrastructure that help share knowledge globally without barriers, the Open Publishing Awards seek to recognize the contributions of individuals and organizations that advance open in publishing.

A moment for reflection and chance for celebration, the Open Publishing Awards bring together people, projects, and organizations from around the world to shine a light on one another.
