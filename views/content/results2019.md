---
layout: "resultsPerYear.njk"
title: "Open Publishing Awards 2019"
year: "2019"
permalink: "archives/{{year}}/"
results: true
---


Below are the results of the 2019 Open Publishing Awards. We present 11 amazing projects that we hope you both learn from and help us celebrate.

The intention of the awards is to bring stories to you about the amazing diversity of open projects that exist today in publishing. If you tweet, blog, or you are a reporter or researcher we hope that you will join us and use your tools to explore and amplify each of these amazing stories about open publishing to the world.

Show them some love!