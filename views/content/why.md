---
title: why
tags: why
permalink: false
---

## About the Open Publishing Awards

The Open Publishing Awards celebrate software and content in publishing that use open licenses but also, importantly, provide a chance to reflect on the strategic value of openness. Why is ‘open’ valuable in the publishing sector?
If you made a nomination, you may have noted the question: “What role does ‘open’ play in making this project special?” Our hope is that you will take the time to consider this question, and make the case for why open is important for the project or projects you are nominating.

Too often, ‘open’ is reduced to discussions of licenses and then contrasted against proprietary business models as a risky alternative to legacy closed models. These arguments often treat the issue shallowly as if ‘open’ is some kind of hippy dippy, lovey dovey, hobbyist pastime. These critiques miss the real-world power of open. They fail to understand that open means more than ‘non-proprietary’, that open is a powerful tool to build collaboration, trust, adoption, to share the cost and burden of creating these works, to diffuse innovations into the market, to learn from each other, and to improve the quality and impact of the works themselves.

These cases need to be articulated and shared. Not just to counter the critiques, but so we can learn from each other.
This is one of the main reasons why the Open Publishing Awards exist, along with celebrating the many amazing open projects in this sector.

Its also important to note that while we are using the framing of ‘awards’, this is not a competition. Sure, projects will be called out for special mention, but we are avoiding terminology such as ‘winners’ and ‘best of’. The Awards are a useful mechanism to hold up some of the open projects in this domain and celebrate them together. It’s about community, not competition.

Please make as many nominations as you can. We appreciate thoughtful answers to the questions.

This year we will also be looking for opportunities to promote the selected projects as much as we can, this will include an invitation to each short listed project to be part of the 2021 Open Publishing Fest to present their work.

<figure class="signature-img">
    <img src="images/adam.jpg" />
    <img class="coko-img" src="images/coko.png" />
</figure>


Adam, [Coko Founder](https://coko.foundation)  
_adam@coko.foundation_

