---
title: Nominate a project for the Open Publishing Awards 2021
class: form 
permalink: false
---



Hello there!

You can now propose a project to be reviewed by the judges, and maybe become a recipient of the Open Publishing Award 2021!

Please fill the form below, and we’ll get in touch with the folks behind the project.
