---
layout: "resultsPerYear.njk"
title: "Open Publishing Awards 2021"
year: "2021"
permalink: "/results/2021/index.html"
judgetext: "Meet the judges for the 2021 edition!"
results: true
---

<!-- The awardees list for 2021 is coming up!
Be sure to be part of the [ceremony](https://force2021.sched.com/event/pxQZ/closing-remarks-open-publishing-awards-ceremony) on the evening (9pm UTC) of the 9 december 2021! -->


Below are the results of the 2021 Open Publishing Awards. We present amazing projects that we hope you both learn from and help us celebrate.

The intention of the awards is to bring stories to you about the amazing diversity of open projects that exist today in publishing. If you tweet, blog, or you are a reporter or researcher we hope that you will join us and use your tools to explore and amplify each of these amazing stories about open publishing to the world.

Show them some love!