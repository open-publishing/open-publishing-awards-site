---
tags: judging
title: How Judging Works
permalink: false
---

## How Judging Works

We aim to have a large judging group to bring a diversity of perspectives to the process, and to split the work up a little to lessen the amount of work on any individual judge.
Judges are yet to be announced for 2021. The awards are again chaired by last years chair Cameron Neylon.

Once nominations close Cameron will facilitate the creation of a short list.
Judges will then review the shortlist in their own time. Coko staff will be on hand to help research any issues that may come up on an as-needed basis.

Judges will recuse themselves from any nomination where they may be a stakeholder or have a conflict of interest of any kind. We also do not require each judge to look at all entries as they, like you, are very busy people.

Cameron will compile the reviews from the judges and facilitate the creation of the final list of recipients. These recipients will then be announced at a date yet to be announced.

Note: Coko founded the awards and pays the bills and provides all the logistics (website design, announcements etc). Consequently, to preserve the neutrality of the awards Coko projects cannot be nominated.
