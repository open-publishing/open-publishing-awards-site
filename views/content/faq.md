---
title: Frequently Asked Questions
tags: faq
permalink: false
---

## Frequently Asked Questions

### Who can submit nominations?
Anyone who is enamored of a project or work that fits the criteria for either of our two categories this year. Self-nomination is welcome and encouraged. The awards are organized by Coko, so any such projects founded by Coko or Coko staff are not eligible. Explicitly this means the following projects cannot enter – Coko itself, PubSweet, Wax, XSweet, Paged.js, Editoria, PagedMedia, Booktype, FLOSS Manuals, Cabbage Tree Labs, Book Sprints PubSource, and any books / articles published by Coko and its staff.

### Can I nominate my own project?

Yes! Please do!

### How do I submit a nomination?

Visit our web form, select the category your submission belongs in, and answer the required questions with as much detail as possible for the judges to review.

### When do nominations close?

To be announced.

### How many nominations can I make?

Please nominate as many projects as you like!

### When will the results be announced?

We will post the dates shortly!

### Can Public Domain materials be considered?

Yes. For the Open Content category we will use the Open Knowledge definition of open as a guiding principle:
“[Open Content] can be freely used, modified, and shared by anyone for any purpose”